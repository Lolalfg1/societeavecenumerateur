﻿using SocieteAvecEnumerateur.Structures;
using System;

namespace SocieteAvecEnumerateur
{
    class Program
    {
        static void Main(string[] args)
        {
            Employes employe1 = new Employes(1200, "Stern", "Alex", 20);
            Employes employe2 = new Employes(1580, "Starkov", "Alina", 26);
            Employes employe3 = new Employes(1800, "Potter", "Harry", 27);
            Employes employe4 = new Employes(1540, "Wilde", "Ivy", 30);
            Employes employe5 = new Employes(1300, "Oretsev", "Mal", 26);
            Chef chef1 = new Chef("Informatique", 2100, "Arlington", "Daniel", 24);
            Chef chef2 = new Chef("Ressources humaines", 1800, "Summer", "Buffy", 25);
            Directeur directeur1 = new Directeur("Poudlar", "Ressources humaines", 3000, "Dumbledore", "Albus", 60);


            Console.WriteLine("Insertion des éléments à la liste");

            Liste personnes = new Liste();
            personnes.InsererDebut(directeur1);
            personnes.InsererDebut(chef2);
            personnes.InsererDebut(chef1);
            personnes.InsererDebut(employe5);
            personnes.InsererDebut(employe4);
            personnes.InsererDebut(employe3);
            personnes.InsererDebut(employe2);
            personnes.InsererDebut(employe1);

            Console.WriteLine("Nombre d'éléments : {0}", personnes.NbElements);
            Console.WriteLine("Avec indexeur");
            for (int i = 0; i < personnes.NbElements; i++)
            {
                Console.WriteLine(personnes[i].Objet.ToString());
            }

            ListeEnumeration personnesEnumeration = new ListeEnumeration(personnes);
            Console.WriteLine("Nombre d'éléments : {0}", personnes.NbElements);
            Console.WriteLine("Avec enumerator");
            do
            {
                Console.WriteLine(personnesEnumeration.Current().ToString());
            }
            while (personnesEnumeration.MoveNext());
        }
    }
}
