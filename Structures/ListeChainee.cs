﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocieteAvecEnumerateur.Structures
{
    public class Liste
    {
        private Element _Debut;
        private int _NbElements;

        private Element[] a = new Element[100];

        public void FillArray()
        {
            Element el = this._Debut;
            if (el != null)
            {
                int i = 0;
                a[i] = el;
                while (el.Suivant != null)
                {
                    el = el.Suivant;
                    i++;
                    a[i] = el;
                }
            }
        }

        public Element this[int i]
        {
            get => a[i];
            set => a[i] = value;
        }

        public int NbElements { get => _NbElements; }

        public Liste()
        {
            _Debut = null;
            _NbElements = 0;
        }
        public void InsererDebut(object premier_objet)
        {
            Element newElDebut = new Element(premier_objet);
            newElDebut.Suivant = this._Debut;
            this._Debut = newElDebut;
            this._NbElements++;
            FillArray();
        }

        public void InsererFin(object dernier_objet)
        {
            Element newElFin = new Element(dernier_objet);
            if (this._Debut == null)
            {
                this._Debut = newElFin;
                this._NbElements++;
                return;
            }
            Element dernierElement = RecupereDernierElement();
            dernierElement.Suivant = newElFin;
            this._NbElements++;
            FillArray();
        }

        public Element RecupereDernierElement()
        {
            Element el = this._Debut;
            while (el.Suivant != null)
            {
                el = el.Suivant;
            }
            return el;
        }

        public void Lister()
        {
            Element el = this._Debut;
            if (el != null)
            {
                string cumul = el.Objet.ToString();
                while (el.Suivant != null)
                {
                    el = el.Suivant;
                    cumul += "," + el.Objet.ToString();
                }
                Console.WriteLine(cumul);
            }
            else
            {
                Console.WriteLine("La liste est vide");
            }
        }

        public void Vider()
        {
            this._Debut = null;
            this._NbElements = 0;
            FillArray();
        }

    }
    public class Element
    {
        private object _Object;
        private Element _Suivant;

        public Element(object _object)
        {
            this._Object = _object;
        }

        public object Objet { get => _Object; set => _Object = value; }
        public Element Suivant { get => _Suivant; set => _Suivant = value; }
    }
}
