﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocieteAvecEnumerateur.Structures
{
    public class Employes : Personne
    {
        public double Salaire { get; set; }


        public Employes(double salaire, string nom, string prenom, int age) : base(nom, prenom, age)
        {
            this.Salaire = salaire;
        }

        public override string ToString()
        {
            return $"{this.Nom} {this.Prenom} {this.Age} {this.Salaire}";
        }

        public void Afficher(Employes employes)
        {
            Console.WriteLine($"Nom:{employes.Nom}");
            Console.WriteLine($"Prenom:{employes.Prenom}");
            Console.WriteLine($"Age:{employes.Age}");
            Console.WriteLine($"Salaire:{employes.Salaire}");
            Console.WriteLine("");


        }

        public void Afficher()
        {
            Console.WriteLine($"Nom:{this.Nom}");
            Console.WriteLine($"Prenom:{this.Prenom}");
            Console.WriteLine($"Age:{this.Age}");
            Console.WriteLine($"Salaire:{this.Salaire}");
            Console.WriteLine("");


        }

    }
}
