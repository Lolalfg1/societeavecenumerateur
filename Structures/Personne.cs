﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SocieteAvecEnumerateur.Structures
{
   public class Personne
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int Age { get; set; }


        public Personne(string nom, string prenom, int age)
        {
            this.Nom = nom;
            this.Prenom = prenom;
            this.Age = age;
        }

        public override string ToString()
        {
            return $"{this.Nom} {this.Prenom} {this.Age}";
        }

        public void Afficher(Personne personne)
        {
            Console.WriteLine($"Nom:{personne.Nom}");
            Console.WriteLine($"Prenom:{personne.Prenom}");
            Console.WriteLine($"Age:{personne.Age}");
            Console.WriteLine("");


        }

        public void Afficher()
        {
            Console.WriteLine($"Nom:{this.Nom}");
            Console.WriteLine($"Prenom:{this.Prenom}");
            Console.WriteLine($"Age:{this.Age}");
            Console.WriteLine("");


        }

    }
}
